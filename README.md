# scripts
 A collection of scripts I like to keep handy

 * [autoUpdateTex](autoUpdateTex.sh) A simple script that rebuilds a
    latex pdf every 30 seconds
 * [changeMeWall](changeMeWall.sh) Changes the wallpaper to a random one in the
    `~/Pictures/Wallpapers` folder.
     * *[changeMeWallCicle](changeMeWallCicle.sh) runs this every 5 minutes*
